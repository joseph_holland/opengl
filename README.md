# OpenGL

OpenGL renderer using C++ with GLFW.

## Prerequisites

- GCC compiler
- Make
- GLFW x64 3.3.7
- Glad OpenGL 3.3 Core

## Compiling and running

See the Makefile. Run by using `make` in root