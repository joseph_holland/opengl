OBJS = main.cpp src\glad.c

CC = g++

# INCLUDE_PATHS = -IC:\msys64\mingw64\include\GLFW -IC:\msys64\mingw64\include\glad
# LIB_PATHS = -LC:\msys64\mingw64\lib

# Compiler Flags
# -w suppresses all warnings
COMPILER_FLAGS = -w

LINKER_FLAGS = -lglfw3 -lgdi32 -lopengl32

EXE_NAME = opengl

all : $(OBJS)
		$(CC) $(OBJS) $(INCLUDE_PATHS) $(LIB_PATHS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(EXE_NAME)
		$(EXE_NAME)